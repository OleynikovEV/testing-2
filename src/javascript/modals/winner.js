import { showModal } from './modal';
import { createFighterDetails } from './fighterDetails';

export function showWinnerModal(fighter) {
    const title = 'Winner';
    const bodyElement = createFighterDetails(fighter);
    showModal({ title, bodyElement });
}
