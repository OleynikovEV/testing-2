import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

export function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'div', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'div', className: 'fighter-attack' });
  const defensekElement = createElement({ tagName: 'div', className: 'fighter-defense' });
  const healthkElement = createElement({ tagName: 'div', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image' });

  imageElement.src = source;
  fighterDetails.append(imageElement);
  fighterDetails.append( createLine(nameElement, 'Name', name) );
  fighterDetails.append( createLine(attackElement, 'Attack', attack) );
  fighterDetails.append( createLine(defensekElement, 'Defensek', defense) );
  fighterDetails.append( createLine(healthkElement, 'Health', health) );

  return fighterDetails;
}

function createLine(element, title, text) {
  const titleElement = createElement({ tagName: 'span', className: 'bold' });
  const bodyElement = createElement({ tagName: 'span', className: 'text' });

  titleElement.innerText = title;
  bodyElement.innerText = text;

  element.append(titleElement);
  element.append(bodyElement);

  return element;
}
