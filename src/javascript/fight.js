export function fight(firstFighter, secondFighter) {
    let finish = false;
    let firstFighter_health = firstFighter.health;
    let secondFighter_health = secondFighter.health;

    do {
        secondFighter_health -= getDamage(firstFighter, secondFighter);
        firstFighter_health -= getDamage(secondFighter, firstFighter);

        if (firstFighter_health <= 0 || secondFighter_health <= 0) {
            finish = true;
        }
    } while (finish == false);

    if (firstFighter_health > 0) return firstFighter;
    if (secondFighter_health > 0) return secondFighter;
}

export function getDamage(attacker, enemy) {
    let block = getBlockPower(enemy);
    let attack = getHitPower(attacker);
    if (block > attack) block = 0;

    return attack - block;
}

export function getHitPower(fighter) {
    const criticalHitChance = Math.random() * 1;
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance = Math.random() * 1;
    return fighter.defense * dodgeChance;
}
